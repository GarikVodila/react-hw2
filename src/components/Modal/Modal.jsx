import React from 'react';
import PropTypes from 'prop-types';

import './Modal.scss';

import { ReactComponent as Close } from './close-icon.svg';

function Modal({ title, children, handleModal, handleOutside, addToCart, articleForCart }) {

    return (
        <div className="modal" onClick={e => handleOutside(e)}>
            <div className="modal__content">
                <div className="modal__header">
                    <div className="modal__title">{title}</div>
                    <Close className="modal__close" onClick={() => handleModal()} />
                </div>
                <div className="modal__body">{children}</div>
                <div className="modal__footer">
                    <button className="confirm"
                        onClick={() => {
                            addToCart(articleForCart);
                            handleModal();
                        }}
                    >
                        Confirm
                    </button>
                    <button className="cancel" onClick={() => handleModal()}>Cancel</button>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.any,
    handleModal: PropTypes.func,
    handleOutside: PropTypes.func,
    addToCart: PropTypes.func,
    articleForCart: PropTypes.number,
};

export default Modal;
