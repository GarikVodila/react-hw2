import React from 'react';
import PropTypes from 'prop-types';

import ListItems from './ListItems/ListItems';

import './Main.scss';

function Main({ listItems, favorites, cart, addToFavorites, addToCart }) {
    return (
        <div className="main__wrapper">
            <div className="container">
                <div className="main">
                    <h1 className="main__title">ALL SHOES</h1>
                    <div className="main__content">
                        <ListItems
                            listItems={listItems}
                            favorites={favorites}
                            cart={cart}
                            addToFavorites={addToFavorites}
                            addToCart={addToCart}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

Main.propTypes = {
    listItems: PropTypes.array,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    addToFavorites: PropTypes.func,
    addToCart: PropTypes.func,
};

export default Main;
