import React from 'react';
import './App.css';

import { Header } from './components/Header';
import { Main } from './components/Main';
import { sendRequest } from './helpers/sendRequest/';

function App() {
    const [listItems, setListItems] = React.useState([]);
    const [favorites, setFavorites] = React.useState([]);
    const [cart, setCart] = React.useState([]);
    const [qtyFavorites, setQtyFavorites] = React.useState(favorites.length);
    const [qtyCart, setQtyCart] = React.useState(cart.length);

    React.useEffect(() => {
        sendRequest('/data.json').then(data => setListItems(data));

        const savedFavorites = localStorage.getItem('favorites');
        const savedCart = localStorage.getItem('cart');

        if (savedFavorites) {
            setFavorites(JSON.parse(savedFavorites));
        }

        if (savedCart) {
            setCart(JSON.parse(savedCart));
        }
    }, []);

    console.log(favorites);
    console.log(cart);

    React.useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
        setQtyFavorites(favorites.length);
    }, [favorites]);

    React.useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        setQtyCart(cart.length);
    }, [cart]);

    const addToFavorites = article => {
        if (!favorites.includes(article)) {
            setFavorites([...favorites, article]);
        } else {
            const indexArticle = favorites.indexOf(article);
            favorites.splice(indexArticle, 1);
            setFavorites([...favorites]);
        }
    };

    const addToCart = article => {
        if (!cart.includes(article)) {
            setCart([...cart, article]);
            setQtyCart(qty => qty + 1);
        }
    };

    return (
        <div className="app-wrapper">
            <Header qtyFavorites={qtyFavorites} qtyCart={qtyCart} />
            <Main listItems={listItems} favorites={favorites} cart={cart} addToFavorites={addToFavorites} addToCart={addToCart} />
        </div>
    );
}

export default App;
